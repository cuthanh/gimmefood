import DataLoader from 'dataloader';
import findByIds from 'mongo-find-by-ids';

export default class Food {
  constructor(context) {
    this.context = context;
    this.collection = context.db.collection('foodorder');
    this.pubsub = context.pubsub;
    this.loader = new DataLoader(ids => findByIds(this.collection, ids));
  }

  findOneById(id) {
    return this.loader.load(id);
  }

  all({ lastCreatedAt = 0, limit = 10 }) {
    return this.collection.find({
      createdAt: { $gt: lastCreatedAt },
    }).sort({ createdAt: 1 }).limit(limit).toArray();
  }

  food(foodOrder) {
    return this.context.Food.collection.find({
      _id: foodOrder.foodId
    }).toArray();
  }

  async insert(doc) {
    const docToInsert = Object.assign({ status: 'WAITING'}, doc, {
      createdAt: Date.now(),
      updatedAt: Date.now(),
    });
    console.log(docToInsert)
    const id = (await this.collection.insertOne(docToInsert)).insertedId;
    this.pubsub.publish('foodOrderInserted', await this.findOneById(id));
    return id;
  }

  async updateById(id, doc) {
    const ret = await this.collection.update({ _id: id }, {
      $set: Object.assign({}, doc, {
        updatedAt: Date.now(),
      }),
    });
    this.loader.clear(id);
    this.pubsub.publish('foodOrderUpdated', await this.findOneById(id));
    return ret;
  }

  async removeById(id) {
    const ret = this.collection.remove({ _id: id });
    this.loader.clear(id);
    this.pubsub.publish('foodOrderRemoved', id);
    return ret;
  }
}
