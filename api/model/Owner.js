import DataLoader from 'dataloader';
import findByIds from 'mongo-find-by-ids';
import bcrypt from 'bcrypt';
// Set this as appropriate
const SALT_ROUNDS = 10; // TODO: ADD this to config file

export default class Owner {
  constructor(context) {
    this.context = context;
    this.collection = context.db.collection('owner');
    this.pubsub = context.pubsub;
    this.loader = new DataLoader(ids => findByIds(this.collection, ids));
  }

  findOneById(id) {
    return this.loader.load(id);
  }

  all({ lastCreatedAt = 0, limit = 10 }) {
    return this.collection.find({
      createdAt: { $gt: lastCreatedAt },
    }).sort({ createdAt: 1 }).limit(limit).toArray();
  }

  store(owner) {
    return this.context.Store.findOneById(owner.storeId);
  }

  async insert(doc) {
    const { password, email, ...rest } = doc;
    const checker = await this.collection.find({ email }).toArray()
    if (checker.length > 0) throw new Error("This email already in use. Try the others!")
    const hash = await bcrypt.hash(password, SALT_ROUNDS);

    // Create new store when insert
    const store = await this.context.Store.insert({ name: `${doc.name}'s quán`})

    const docToInsert = Object.assign({}, rest, {
      email,
      hash,
      storeId: store,
      createdAt: Date.now(),
      updatedAt: Date.now(),
    });
    const id = (await this.collection.insertOne(docToInsert)).insertedId;
    this.pubsub.publish('ownerInserted', await this.findOneById(id));
    return id;
  }

  async updateById(id, doc) {
    const ret = await this.collection.update({ _id: id }, {
      $set: Object.assign({}, doc, {
        updatedAt: Date.now(),
      }),
    });
    this.loader.clear(id);
    this.pubsub.publish('ownerUpdated', await this.findOneById(id));
    return ret;
  }

  async removeById(id) {
    const ret = this.collection.remove({ _id: id });
    this.loader.clear(id);
    this.pubsub.publish('ownerRemoved', id);
    return ret;
  }
}
