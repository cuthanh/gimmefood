const models = {};

export default function addModelsToContext(context) {
  const newContext = Object.assign({}, context);
  Object.keys(models).forEach((key) => {
    newContext[key] = new models[key](newContext);
  });
  return newContext;
}

import Admin from './Admin';
models.Admin = Admin;

import Customer from './Customer';
models.Customer = Customer;

import Food from './Food';
models.Food = Food;

import Owner from './Owner';
models.Owner = Owner;

import Store from './Store';
models.Store = Store;

import Order from './Order';
models.Order = Order;

// import FoodOrder from './FoodOrder';
// models.FoodOrder = FoodOrder;
