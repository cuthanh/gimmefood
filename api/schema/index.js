import fs from 'fs';

function requireGraphQL(name) {
  const filename = require.resolve(name);
  return fs.readFileSync(filename, 'utf8');
}

const typeDefs = [`
  scalar ObjID
  type Query {
    # A placeholder, please ignore
    __placeholder: Int
  }
  type Mutation {
    # A placeholder, please ignore
    __placeholder: Int
  }
  type Subscription {
    # A placeholder, please ignore
    __placeholder: Int
  }
`];

export default typeDefs;

typeDefs.push(requireGraphQL('./Auth.graphql'));

typeDefs.push(requireGraphQL('./Customer.graphql'));

typeDefs.push(requireGraphQL('./FoodStatus.graphql'));

typeDefs.push(requireGraphQL('./FoodOrderStatus.graphql'));

typeDefs.push(requireGraphQL('./Food.graphql'));

typeDefs.push(requireGraphQL('./Owner.graphql'));

typeDefs.push(requireGraphQL('./Store.graphql'));

typeDefs.push(requireGraphQL('./Order.graphql'));

typeDefs.push(requireGraphQL('./FoodOrder.graphql'));

typeDefs.push(requireGraphQL('./Admin.graphql'));
