import { ObjectId } from 'mongodb';
import { GraphQLScalarType } from 'graphql';
import { Kind } from 'graphql/language';
import { merge } from 'lodash';

const resolvers = {};

resolvers.ObjID = new GraphQLScalarType({
  name: 'ObjID',
  description: 'Id representation, based on Mongo Object Ids',
  parseValue(value) {
    return ObjectId(value);
  },
  serialize(value) {
    return value.toString();
  },
  parseLiteral(ast) {
    if (ast.kind === Kind.STRING) {
      return ObjectId(ast.value);
    }
    return null;
  },
});

export default resolvers;

import customerResolvers from './Customer';
merge(resolvers, customerResolvers);

import foodResolvers from './Food';
merge(resolvers, foodResolvers);

import ownerResolvers from './Owner';
merge(resolvers, ownerResolvers);

import storeResolvers from './Store';
merge(resolvers, storeResolvers);

import orderResolvers from './Order';
merge(resolvers, orderResolvers);

import authResolvers from './Auth';
merge(resolvers, authResolvers);

import foodOrderResolvers from './FoodOrder';
merge(resolvers, foodOrderResolvers);
