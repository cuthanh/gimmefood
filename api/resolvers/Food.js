const resolvers = {
  Food: {
    id(food) {
      return food._id;
    },
  },
  OwnerPoint: {
    foods(root, arg, { Food, user }) {
      return Food.ourFood(user.storeId)
    },

    food(root, { id }, { Food }) {
      return Food.findOneById(id);
    },
  },
  CustomerPoint: {
    food(root, { id }, { Food }) {
      return Food.findOneById(id);
    },
  },
  Mutation: {
    async createFood(root, { input }, { Food }) {
      const id = await Food.insert(input);
      return Food.findOneById(id);
    },

    async updateFood(root, { id, input }, { Food }) {
      await Food.updateById(id, input);
      return Food.findOneById(id);
    },

    removeFood(root, { id }, { Food }) {
      return Food.removeById(id);
    },
  },
  Subscription: {
    foodCreated: food => food,
    foodUpdated: food => food,
    foodRemoved: id => id,
  },
};

export default resolvers;
