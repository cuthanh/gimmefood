const resolvers = {
  Owner: {
    id(owner) {
      return owner._id;
    },

    store(owner, args, { Owner }) {
      return Owner.store(owner);
    },
  },
  AdminPoint: {
    owners(root, { lastCreatedAt, limit }, { Owner }) {
      return Owner.all({ lastCreatedAt, limit });
    },

    owner(root, { id }, { Owner }) {
      return Owner.findOneById(id);
    },
  },
  Mutation: {
    async createOwner(root, { input }, { Owner }) {
      const id = await Owner.insert(input);
      return Owner.findOneById(id);
    },

    async updateOwner(root, { id, input }, { Owner }) {
      await Owner.updateById(id, input);
      return Owner.findOneById(id);
    },

    removeOwner(root, { id }, { Owner }) {
      return Owner.removeById(id);
    },
  },
  Subscription: {
    ownerCreated: owner => owner,
    ownerUpdated: owner => owner,
    ownerRemoved: id => id,
  },
};

export default resolvers;
