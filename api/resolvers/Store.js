const resolvers = {
  Store: {
    id(store) {
      return store._id;
    },

    menu(store, { lastCreatedAt, limit }, { Store }) {
      return Store.menu(store, { lastCreatedAt, limit });
    },
  },
  AdminPoint: {
    stores(root, { lastCreatedAt, limit }, { Store }) {
      return Store.all({ lastCreatedAt, limit });
    },

    store(root, { id }, { Store }) {
      return Store.findOneById(id);
    },
  },
  OwnerPoint: {
    store(root, { id }, { Store, user }) {
      console.log(user)
      if (user.storeId == null) return null
      return Store.findOneById(user.storeId);
    },
  },
  CustomerPoint: {
    store(root, { id }, { Store }) {
      return Store.findOneById(id);
    },
  },
  Mutation: {
    async createStore(root, { input }, { Store }) {
      const id = await Store.insert(input);
      return Store.findOneById(id);
    },

    async updateStore(root, { id, input }, { Store }) {
      await Store.updateById(id, input);
      return Store.findOneById(id);
    },

    removeStore(root, { id }, { Store }) {
      return Store.removeById(id);
    },
  },
  Subscription: {
    storeCreated: store => store,
    storeUpdated: store => store,
    storeRemoved: id => id,
  },
};

export default resolvers;
