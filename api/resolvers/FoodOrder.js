const resolvers = {
  FoodOrder: {
    food(foodOrder, arg, { Food }) {
      return Food.findOneById(foodOrder.foodId)
    }
  },
  // Mutation: {
  //   async addToOrder(root, { input }, { FoodOrder }) {
  //     const id = await FoodOrder.insert(input);
  //     return FoodOrder.findOneById(id);
  //   },
  // },
};

export default resolvers;
