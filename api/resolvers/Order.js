const resolvers = {
  Order: {
    id(order) {
      return order._id;
    },
    store(order, args, { Order }) {
      return Order.store(order);
    },
  },
  OwnerPoint: {
    orders(root, { lastCreatedAt, limit }, { Order, user }) {
      return Order.findByOwner(user.storeId)
    },

    order(root, { id }, { Order }) {
      return Order.findOneById(id);
    },
  },
  CustomerPoint: {
    orders(root, { lastCreatedAt, limit }, { Order }) {
      return Order.all({ lastCreatedAt, limit });
    },

    order(root, { id }, { Order }) {
      return Order.findOneById(id);
    },
  },
  Mutation: {
    async createOrder(root, { input }, { Order }) {
      const id = await Order.insert(input);
      return Order.findOneById(id);
    },

    async updateOrder(root, { id, input }, { Order }) {
      await Order.updateById(id, input);
      return Order.findOneById(id);
    },

    removeOrder(root, { id }, { Order }) {
      return Order.removeById(id);
    },
  },
  Subscription: {
    orderCreated: order => order,
    orderUpdated: order => order,
    orderRemoved: id => id,
  },
};

export default resolvers;
