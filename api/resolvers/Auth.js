import jwt from 'jsonwebtoken';
import bcrypt from 'bcrypt';
import { ObjectId } from 'mongodb';
import config from '../server/config';

const resolvers = {
  Query: {
    async adminToken(obj, { input: { email, password } }, { Author }) {
      const user = await Author.collection.findOne({ email });
      if (!user || !(await bcrypt.compare(password, user.hash))) {
        throw new Error('User not found or matching email/password combination');
      }
      const payload = {
        userId: user._id.toString(),
      };
      const token = jwt.sign(payload, config.secretKey, { expiresIn: '7d' });
      return token;
    },
    async ownerToken(obj, { input: { email, password } }, { Owner }) {
      const user = await Owner.collection.findOne({ email });
      if (!user || !(await bcrypt.compare(password, user.hash))) {
        throw new Error('User not found or matching email/password combination');
      }
      const payload = {
        userId: user._id.toString(),
      };
      const token = jwt.sign(payload, config.secretKey, { expiresIn: '7d' });
      return token;
    },
    async customerToken(obj, { input: { email, password } }, { Customer }) {
      const user = await Customer.collection.findOne({ email });
      if (!user || !(await bcrypt.compare(password, user.hash))) {
        throw new Error('User not found or matching email/password combination');
      }
      const payload = {
        userId: user._id.toString(),
      };
      const token = jwt.sign(payload, config.secretKey, { expiresIn: '7d' });
      return token;
    },
    async admin(root, { token }, context) {
      // JUST A HARD CODE
      // const payload = jwt.verify(token, config.secretKey);
      // const user = await context.Admin.findOneById(ObjectId(payload.userId));
      // context.user = user;
      // return { me: user };

      const user = { email: 'admin@gimmefood.com' }
      context.user = user;
      if (token === 'ADMIN_TOKEN') {
        return { me: user };
      } else
        throw new Error('Wrong token. Try ADMIN_TOKEN');
    },
    async owner(root, { token }, context) {
      const payload = jwt.verify(token, config.secretKey);
      const user = await context.Owner.findOneById(ObjectId(payload.userId));
      context.user = user;
      return { me: user };
    },
    async customer(root, { token }, context) {
      const payload = jwt.decode(token, config.secretKey);
      if (payload) {
        const user = await context.Customer.findOneById(ObjectId(payload.userId));
        context.user = user;
        return { me: user };
      }
      return {me: {
        _id: 'justanrandomidforanonymoususerhere',
      }}
    },
  },
  Mutation: {
    async login(root, { token }, context) {
      const payload = jwt.verify(token, config.secretKey);
      const user = await context.Customer.findOneById(ObjectId(payload.userId));
      context.user = user;
      return user;
    }
  }
};

export default resolvers;
