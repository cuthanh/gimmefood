import Vue from "vue";
import Router from "vue-router";

// Component
import OrderList from "@/components/OrderList";
import Menu from "@/components/Menu";
import Editfood from "@/components/Editfood"
import Store from "@/components/Store";
import EditOwner from "@/components/EditOwner"
import OwnerList from "@/components/OwnerList";
import FoodList from "@/components/FoodList"

import Createfood from "@/components/Createfood";
// Containers
import Full from "@/containers/Full";

// Views
import Dashboard from "@/views/Dashboard";
import Charts from "@/views/Charts";
import Widgets from "@/views/Widgets";

// Views - Components
import Buttons from "@/views/components/Buttons";
import SocialButtons from "@/views/components/SocialButtons";
import Cards from "@/views/components/Cards";
import Forms from "@/views/components/Forms";
import Modals from "@/views/components/Modals";
import Switches from "@/views/components/Switches";
import Tables from "@/views/components/Tables";

// Views - Icons
import FontAwesome from "@/views/icons/FontAwesome";
import SimpleLineIcons from "@/views/icons/SimpleLineIcons";

// Views - Pages
import Page404 from "@/views/pages/Page404";
import Page500 from "@/views/pages/Page500";
import Login from "@/views/pages/Login";
import Register from "@/views/pages/Register";

Vue.use(Router);

const router = new Router({
  mode: "hash", // Demo is living in GitHub.io, so required!
  linkActiveClass: "open active",
  scrollBehavior: () => ({ y: 0 }),
  routes: [
    {
      path: "/",
      redirect: "/dashboard",
      name: "Home",
      component: Full,
      children: [
        {
          path: "dashboard",
          name: "Dashboard",
          component: Dashboard
        },
        {
          path: "/CreateFood",
          name: "Createfood",
          component: Createfood
        },
        {
          path: "/OrderList",
          name: "OrderList",
          component: OrderList
        },
        {
          path: "/Menu",
          name: "Menu",
          component: Menu
        },
        {
          path: "/FoodList/:orderId",
          name: "FoodList",
          component: FoodList
        },
        {
          path: "/Editfood/:id",
          name: "Editfood",
          component:Editfood
        },
        {
          path: "/EditOwner/:index",
          name: "EditOwner",
          component: EditOwner
        },
        {
          path: "/Store",
          name: "Store",
          component: Store
        },
        {
          path: "/OwnerList",
          name: "OwnerList",
          component: OwnerList
        },
      ]
    },
    {
      path: "/pages",
      redirect: "/pages/p404",
      name: "Pages",
      component: {
        render(c) {
          return c("router-view");
        }
      },
      children: [
        {
          path: "404",
          name: "Page404",
          component: Page404
        },
        {
          path: "500",
          name: "Page500",
          component: Page500
        },
        {
          path: "login",
          name: "Login",
          component: Login
        },
        {
          path: "register",
          name: "Register",
          component: Register
        }
      ]
    }
  ]
});

router.beforeEach((to, from, next) => {
  const user = JSON.parse(window.localStorage.getItem("user"));
  if (!user && to.name !== "Login") {
    if(to.name !== "Register"){
      console.log("Hey hey! Pls login")
      next("/pages/login");
    }
    else
      next();
      
  }
  else if(user){
    if(user.name != 'Admin Owner' && to.name === "OwnerList"){
      console.log("You can not see this");
      next("/OrderList");
    }
    else
      next();
  }
  else
    next(); 
});

export default router;
