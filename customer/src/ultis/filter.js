export const money = (input) => {
    input += '';
    return input.replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1.")
}