import Vue from 'vue'
import Router from 'vue-router'
import Hello from '@/components/Hello'
import Store from '@/components/Store'
import CheckIn from '@/components/CheckIn'
import Order from '@/components/Order'
import Home from '@/components/Homepage'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/store/:storeId',
      name: 'Store',
      component: Store
    },
    {
      path: '/checkin/:storeId',
      name: 'CheckIn',
      component: CheckIn
    },
    {
      path: '/order/:orderId',
      name: 'Order',
      component: Order
    },
    {
      path: '/',
      name: 'Home',
      component: Home
    },
  ]
})
